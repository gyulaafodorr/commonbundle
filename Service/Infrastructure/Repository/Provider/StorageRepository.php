<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 16/07/13
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Provider;


use Themaholic\CommonBundle\Entity\StoredEntity;
use Themaholic\CommonBundle\Service\Infrastructure\Repository\Entity\EntityRepositoryInterface;
use Themaholic\CommonBundle\Service\Infrastructure\Storage\StorageInterface;

class StorageRepository implements RepositoryProviderInterface
{

    /**
     * @var StorageInterface
     */
    private $storage;
    private $repositories = array();

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function addRepository($name, EntityRepositoryInterface $repository)
    {
        $this->repositories[$name] = $repository;
    }

    /**
     * @param $entityName
     *
     * @return EntityRepositoryInterface
     */
    public function getRepository($entityName)
    {
        return $this->repositories[$entityName];
    }

    public function save(StoredEntity $entity)
    {
        if (!$entity->getId())
        {
            $reflectionProperty = new \ReflectionProperty($entity, "id");
            $reflectionProperty->setAccessible(TRUE);
            $reflectionProperty->setValue($entity, spl_object_hash($entity));
        }
        $this->storage->store($this->getRepository(get_class($entity))->getkey($entity->getId()), $entity);
    }

    public function delete(StoredEntity $entity)
    {
        $this->storage->delete($this->getRepository(get_class($entity))->getkey($entity->getId()));
    }

    public function detach(StoredEntity $entity)
    {
        return;
    }

    public function flush(StoredEntity $entity = NULL)
    {
        return;
    }

    public function beginTransaction()
    {
        return;
    }

    public function commit()
    {
        return;
    }

    public function rollback()
    {
        return;
    }
}