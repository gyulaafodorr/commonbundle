<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 06/06/13
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Provider;

use Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface;

interface RepositoryProviderInterface extends RepositoryInterface
{

}
