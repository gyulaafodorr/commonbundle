<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 06/06/13
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Provider;
use Themaholic\CommonBundle\Entity\BaseDomainEntity;
use Themaholic\CommonBundle\Entity\StoredEntity;
use Doctrine\Common\Collections\Criteria;

class DoctrineEntityRepository implements RepositoryProviderInterface
{

    /**
     * @var
     */
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function flush(StoredEntity $entity = NULL)
    {
        $this->entityManager->flush($entity);
    }

    public function save(StoredEntity $entity)
    {
        return $this->entityManager->persist($entity);
    }

    public function delete(StoredEntity $entity)
    {
        return $this->entityManager->remove($entity);
    }

    public function detach(StoredEntity $entity)
    {
        return $this->entityManager->detach($entity);
    }

    public function beginTransaction()
    {
        $this->entityManager->beginTransaction();
    }

    public function commit()
    {
        $this->entityManager->commit();
    }

    public function rollback()
    {
        $this->entityManager->rollback();
    }

    public function getRepository($entityName)
    {
        return $this->entityManager->getRepository($entityName);
    }


}