<?php

	namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Configurator;

	use Themaholic\CommonBundle\Service\Infrastructure\Repository\Provider\RepositoryProviderInterface;

	interface RepositoryConfiguratorInterface
	{
		/**
		 * @param $class
		 * @return RepositoryProviderInterface
		 */
		public function getProvider($class);
		public function getProviders();
	}

?>