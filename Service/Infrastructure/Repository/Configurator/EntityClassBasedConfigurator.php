<?php

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Configurator;

use Themaholic\CommonBundle\Service\Infrastructure\Repository\Configurator\RepositoryConfiguratorInterface;
use Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface;

class EntityClassBasedConfigurator implements RepositoryConfiguratorInterface
{
    private $providers = array();
    private $defaultProvider;
    private $mappings = array();

    public function __construct($repository)
    {
        $this->defaultProvider = $repository;
    }

    public function registerProvider($name, $repository)
    {
        if (preg_match("/^[A-Za-z\\\]+$/", $name) !== 1)
        {
            throw new \Exception(sprintf("Invalid repository name '%s'", $name));
        }
        if ($this->providerExists($name))
        {
            throw new \Exception(sprintf("Provider for '%s' already exists", $name));
        }
        $this->providers[$name] = $repository;
    }

    private function providerExists($name)
    {
        return array_key_exists($name, $this->providers);
    }

    public function registerMapping($class, $name)
    {
        if (preg_match("/^[A-Za-z\\\]+$/", $name) !== 1)
        {
            throw new \Exception(sprintf("Invalid class name '%s'", $name));
        }
        if ($this->mappingExists($class))
        {
            throw new \Exception(sprintf("Mapping for '%s' already exists", $class));
        }
        if (!$this->providerExists($name))
        {
            throw new \Exception(sprintf("Provider for '%s' does not exist", $name));
        }
        if (!class_exists($class))
        {
            throw new \Exception(sprintf("Mapped class '%s' does not exist", $class));
        }
        $this->mappings[$class] = $name;
    }

    private function mappingExists($class)
    {
        return array_key_exists($class, $this->mappings);
    }

    private function getMapping($class)
    {
        if (!$this->mappingExists($class))
        {
            throw new \Exception("Mapping '%' does not exist", $class);
        }
        return $this->mappings[$class];
    }

    public function getProvider($class)
    {
        return $this->mappingExists($class) ? $this->providers[$this->getMapping($class)] : $this->defaultProvider;
    }

    public function getProviders()
    {
        return array_values(array_merge(array($this->defaultProvider), $this->providers));
    }
}

?>