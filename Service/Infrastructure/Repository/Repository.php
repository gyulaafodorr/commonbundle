<?php

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository;

use Themaholic\CommonBundle\Entity\BaseDomainEntity;
use Themaholic\CommonBundle\Entity\StoredEntity;
use Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface;
use Themaholic\CommonBundle\Service\Infrastructure\Repository\Configurator\RepositoryConfiguratorInterface;

class Repository implements RepositoryInterface
{
    private $configurator;

    public function __construct(RepositoryConfiguratorInterface $configurator)
    {
        $this->configurator = $configurator;
    }

    /**
     * @param $entityName
     * @return RepositoryProviderInterface
     */
    public function getRepository($entityName)
    {
        return $this->configurator->getProvider($entityName)->getRepository($entityName);
    }

    public function save(StoredEntity $entity)
    {
        return $this->configurator->getProvider(get_class($entity))->save($entity);
    }

    public function delete(StoredEntity $entity)
    {
        return $this->configurator->getProvider(get_class($entity))->delete($entity);
    }

    public function detach(StoredEntity $entity)
    {
        return $this->configurator->getProvider(get_class($entity))->detach($entity);
    }

    public function flush(StoredEntity $entity = NULL)
    {
        if ($entity)
        {
            $this->configurator->getProvider($entity)->flush($entity);
        }
        else
        {
            foreach ($this->configurator->getProviders() as $provider)
            {
                $provider->flush();
            }
        }
    }

    public function beginTransaction()
    {
        foreach ($this->configurator->getProviders() as $provider)
        {
            $provider->beginTransaction();
        }
    }

    public function commit()
    {
        foreach ($this->configurator->getProviders() as $provider)
        {
            $provider->commit();
        }
    }

    public function rollback()
    {
        foreach ($this->configurator->getProviders() as $provider)
        {
            $provider->rollback();
        }
    }


}

?>