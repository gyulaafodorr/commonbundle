<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 13/06/13
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Entity;


use Doctrine\Common\Collections\Selectable;
use Doctrine\Common\Persistence\ObjectRepository;

interface EntityRepositoryInterface extends ObjectRepository, Selectable
{
}