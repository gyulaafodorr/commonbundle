<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 15/07/13
 * Time: 17:40
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository\Entity;

use Themaholic\CommonBundle\Exception\NotImplementedException;
use Themaholic\CommonBundle\Service\Infrastructure\Storage\StorageInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

abstract class AbstractStorageRepository implements EntityRepositoryInterface
{

    protected $storage;
    const REPOSITORY_KEY = "REPOSITORY";

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function getKey($id)
    {
        return self::REPOSITORY_KEY . "_" . md5($this->getClassName()) . "_" . $id;
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param int $id The identifier.
     *
     * @return object The object.
     */
    public function find($id)
    {
        return $this->storage->get($this->getKey($id));
    }

    /**
     * Finds all objects in the repository.
     *
     * @return mixed The objects.
     */
    public function findAll()
    {
        throw new NotImplementedException();
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @throws \UnexpectedValueException
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return mixed The objects.
     */
    public function findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
    {
        throw new NotImplementedException();
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria
     *
     * @return object The object.
     */
    public function findOneBy(array $criteria)
    {
        throw new NotImplementedException();
    }

    /**
     * Returns the class name of the object managed by the repository
     *
     * @return string
     */
    abstract public function getClassName();

    /**
     * Selects all elements from a selectable that match the expression and
     * returns a new collection containing these elements.
     *
     * @param Criteria $criteria
     *
     * @return Collection
     */
    public function matching(Criteria $criteria)
    {
        throw new NotImplementedException();
    }
}