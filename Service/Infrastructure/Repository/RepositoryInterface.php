<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 06/06/13
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Repository;

use Themaholic\CommonBundle\Entity\StoredEntity;
use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;
use Themaholic\CommonBundle\Service\Infrastructure\Repository\Entity\EntityRepositoryInterface;

interface RepositoryInterface extends InfrastructureServiceInterface
{
    /**
     * @param $entityName
     * @return EntityRepositoryInterface
     */
    public function getRepository($entityName);

    public function save(StoredEntity $entity);

    public function delete(StoredEntity $entity);

    public function detach(StoredEntity $entity);

    public function flush(StoredEntity $entity = NULL);

    public function beginTransaction();

    public function commit();

    public function rollback();
}