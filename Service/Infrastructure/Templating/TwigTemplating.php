<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:26
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Templating;


class TwigTemplating implements TemplatingInterface
{

    /**
     * @var
     */
    private $templateEngine;

    public function __construct($templateEngine)
    {
        $this->templateEngine = $templateEngine;
    }

    public function render($template, $data)
    {
        return $this->templateEngine->render($template, $data);
    }
}