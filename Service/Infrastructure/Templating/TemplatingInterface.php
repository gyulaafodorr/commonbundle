<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:26
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Templating;


use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;

interface TemplatingInterface extends InfrastructureServiceInterface
{

} 