<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 18/06/13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Factory\DomainFactory;


use Themaholic\CommonBundle\Service\Infrastructure\Factory\FactoryInterface;

interface DomainFactoryInterface extends FactoryInterface {
	/**
	 * @return \Themaholic\CommonBundle\Entity\BaseDomainEntity|object
	 */
	public function create($entityName);

	/**
	 * @return \Themaholic\CommonBundle\Entity\BaseDomainEntity|object
	 */
	public function createFromDto($entityName, $dto);
}