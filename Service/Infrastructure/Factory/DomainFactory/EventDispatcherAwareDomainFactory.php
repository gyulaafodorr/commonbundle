<?php
	/**
	 * Created by JetBrains PhpStorm.
	 * User: Gyula
	 * Date: 18/06/13
	 * Time: 17:15
	 * To change this template use File | Settings | File Templates.
	 */

	namespace Themaholic\CommonBundle\Service\Infrastructure\Factory\DomainFactory;

	use Themaholic\CommonBundle\Service\Infrastructure\EventDispatcher\CognolinkEventDispatcher;

	class EventDispatcherAwareDomainFactory implements DomainFactoryInterface
	{

		/**
		 * @var CognolinkEventDispatcher
		 */
		private $eventDispatcher;

		function __construct(CognolinkEventDispatcher $eventDispatcher)
		{
			$this->eventDispatcher = $eventDispatcher;
		}

        /**
         * @param $entityName
         * @param $dto
         * @return object
         */
        public function createFromDto($entityName, $dto)
		{
			$entity = $this->create($entityName);
			$domainReflection = new \ReflectionClass($entity);
			$dtoReflection = new \ReflectionClass($dto);

			foreach ($dtoReflection->getProperties() as $dtoProperty)
			{
				if ($domainReflection->hasProperty($dtoProperty->getName()))
				{
					$dtoProperty->setAccessible(TRUE);
					$domainProperty = $domainReflection->getProperty($dtoProperty->getName());
					$domainProperty->setAccessible(TRUE);
					$domainProperty->setValue($entity, $dtoProperty->getValue($dto));
				}
			}
			return $entity;
		}

		public function create($entityName, $params = array())
		{
			$reflect = new \ReflectionClass($entityName);
			array_unshift($this->eventDispatcher, $params);
			$entity = $reflect->newInstanceArgs($params);
			return $entity;
		}
	}
