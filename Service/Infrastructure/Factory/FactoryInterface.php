<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 18/06/13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Factory;


use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;

interface FactoryInterface extends InfrastructureServiceInterface {

}