<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:24
 */

namespace Themaholic\CommonBundle\Service\Infrastructure;

use Themaholic\CommonBundle\Service\ServiceInterface;

interface InfrastructureServiceInterface extends ServiceInterface
{

} 