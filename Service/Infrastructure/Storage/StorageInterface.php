<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 15/07/13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\Storage;


use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;

interface StorageInterface extends InfrastructureServiceInterface
{
    public function get($key);

    public function store($key, $data, $ttl = NULL);

    public function delete($key);
}