<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 15/07/13
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
namespace Themaholic\CommonBundle\Service\Infrastructure\Storage;
class MemcachedStorage implements StorageInterface
{

    private $memcache;
    private $port;
    private $host;
    private $ttl = 0;

    public function __construct($host, $port, $ttl)
    {
        $this->host = $host;
        $this->port = $port;
        $this->ttl = $ttl;
    }

    private function connect()
    {
        $this->memcache = new \Memcache();
        try
        {
            $this->memcache->connect($this->host, $this->port);
        }
        catch (\Exception $e)
        {
            throw new \Exception("Cannot connect to memcached server at '" . $this->host . "', port '" . $this->port . "'");
        }
    }

    public function get($key)
    {
        if ($this->memcache === NULL)
        {
            $this->connect();
        }
        if ($data = $this->memcache->get($key))
        {
            return $data;
        }
        else
        {
            return NULL;
        }
    }

    public function store($key, $data, $ttl = NULL)
    {
        if ($this->memcache === NULL)
        {
            $this->connect();
        }
        if (!$ttl)
        {
            $ttl = $this->ttl;
        }
        if ($this->memcache->set($key, $data, MEMCACHE_COMPRESSED, $ttl) === FALSE)
        {
            throw new \Exception("Unable to store data in memcache");
        }
    }

    public function delete($key)
    {
        if ($this->memcache === NULL)
        {
            $this->connect();
        }
        $this->memcache->delete($key);
    }
}