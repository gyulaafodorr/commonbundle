<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 20/06/13
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\EventBus;

use Themaholic\CommonBundle\Event\ThemaholicEvent;
use Themaholic\CommonBundle\Event\MarkerEvent;
use Themaholic\CommonBundle\Exception\NotFoundException;

class ThemaholicEventBus implements EventBusInterface
{

    /**
     * @var \ArrayObject
     */
    private $bus;

    function __construct()
    {
        $this->bus = new \ArrayObject();
    }

    public function add(ThemaholicEvent $event)
    {
        $this->bus->append($event);
    }

    /**
     * @param null $marker
     *
     * @return \ArrayIterator
     * @throws NotFoundException
     */
    private function streamFromMarker($marker = NULL)
    {
        if (!$marker)
        {
            return $this->bus->getIterator();
        }

        $iterator = $this->bus->getIterator();
        while ($iterator->valid())
        {
            if (!($iterator->current() instanceOf MarkerEvent) || $iterator->current()->getMarkerIdentifier() != $marker) $iterator->next();
            else return $iterator;
        }
        throw new NotFoundException("Invalid marker");
    }

    public function find($eventName, $fromMarker = NULL)
    {
        $hits = array();
        $iterator = $this->streamFromMarker($fromMarker);
        while ($iterator->valid())
        {
            if ($iterator->current()->getName() == $eventName) $hits[] = $iterator->current();
            $iterator->next();
        }
        return $hits;
    }

    public function removeAll($eventName, $fromMarker = NULL)
    {
        $iterator = $this->streamFromMarker($fromMarker);
        while ($iterator->valid())
        {
            if ($iterator->current()->getName() == $eventName) $this->bus->offsetUnset($iterator->key());
            $iterator->next();
        }
    }


    public function clear()
    {
        $this->bus = new \ArrayObject();
    }

    public function isPublished($eventName, $fromMarker = NULL)
    {
        return (count($this->find($eventName, $fromMarker)) > 0) ? TRUE : FALSE;
    }

    public function placeMarker()
    {
        $marker = new MarkerEvent();
        $this->add($marker);
        return $marker->getMarkerIdentifier();
    }
}