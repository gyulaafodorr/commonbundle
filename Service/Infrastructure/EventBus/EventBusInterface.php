<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 20/06/13
 * Time: 14:04
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\EventBus;

use Themaholic\CommonBundle\Event\ThemaholicEvent;
use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;

interface EventBusInterface extends InfrastructureServiceInterface
{
    public function add(ThemaholicEvent $event);

    public function removeAll($eventName, $fromMarker = NULL);

    public function find($eventName, $fromMarker = NULL);

    public function isPublished($eventName, $fromMarker = NULL);

    public function placeMarker();

    public function clear();
}