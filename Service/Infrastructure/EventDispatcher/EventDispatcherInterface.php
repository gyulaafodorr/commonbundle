<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 06/06/13
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\EventDispatcher;


use Themaholic\CommonBundle\Event\ThemaholicEvent;
use Themaholic\CommonBundle\Service\Infrastructure\InfrastructureServiceInterface;

interface EventDispatcherInterface extends InfrastructureServiceInterface
{
	public function dispatch($eventName, ThemaholicEvent $event = NULL);
}