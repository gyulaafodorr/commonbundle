<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 06/06/13
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Infrastructure\EventDispatcher;

use Themaholic\CommonBundle\Event\ThemaholicEvent;
use Themaholic\CommonBundle\Service\Infrastructure\EventBus\EventBusInterface;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

class ThemaholicEventDispatcher implements EventDispatcherInterface
{

    /**
     * @var TraceableEventDispatcher
     */
    private $frameworkEventDispatcher;

    /**
     * @var EventBusInterface
     */
    private $eventBus;

    function __construct($frameworkEventDispatcher, EventBusInterface $eventBus)
    {
        $this->frameworkEventDispatcher = $frameworkEventDispatcher;
        $this->eventBus = $eventBus;
    }

    public function dispatch($eventName, ThemaholicEvent $event = NULL)
    {
        $resultEvent = $this->frameworkEventDispatcher->dispatch($eventName, $event);
        $this->eventBus->add($resultEvent);
        return $resultEvent;
    }


}