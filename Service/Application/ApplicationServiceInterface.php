<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 13/08/13
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Service\Application;

use Themaholic\CommonBundle\Service\ServiceInterface;

interface ApplicationServiceInterface extends ServiceInterface
{

}