<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rudolf.horvath
 * Date: 13/08/13
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */

namespace Cognolink\Trackers\CommonBundle\Service\Application\Domain;


use Cognolink\Trackers\CommonBundle\Service\ServiceInterface;

interface DomainServiceInterface extends ServiceInterface {

}