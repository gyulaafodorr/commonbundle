<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 16/08/13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Aop\Transactional;

use Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use CG\Proxy\MethodInvocation;
use CG\Proxy\MethodInterceptorInterface;

class TransactionalInterceptor implements MethodInterceptorInterface
{
    /**
     * @var \Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface
     */
    private $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function intercept(MethodInvocation $invocation)
    {
        $this->repository->beginTransaction();
        try
        {
            $rs = $invocation->proceed();
            $this->repository->commit();
            return $rs;
        }
        catch (\Exception $e)
        {
            $this->repository->rollback();
            throw $e;
        }
    }
}
