<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 16/08/13
 * Time: 14:07
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Aop\Transactional;

use Doctrine\Common\Annotations\Reader;
use JMS\AopBundle\Aop\PointcutInterface;

class TransactionalPointcut implements PointcutInterface
{
    private $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function matchesClass(\ReflectionClass $class)
    {
        return TRUE;
    }

    public function matchesMethod(\ReflectionMethod $method)
    {
        return NULL !== $this->reader->getMethodAnnotation($method, 'Themaholic\CommonBundle\Aop\Transactional\Transactional');
    }
}

