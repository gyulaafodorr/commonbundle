<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 19/08/13
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Aop\EntityFlush;

use Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use CG\Proxy\MethodInvocation;
use CG\Proxy\MethodInterceptorInterface;

class EntityFlushInterceptor implements MethodInterceptorInterface
{

    /**
     * @var \Themaholic\CommonBundle\Service\Infrastructure\Repository\RepositoryInterface
     */
    private $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function intercept(MethodInvocation $invocation)
    {
        try
        {
            $rs = $invocation->proceed();
            $this->repository->flush();
            return $rs;
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }
}