<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 19/08/13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Aop\EntityFlush;

use Doctrine\Common\Annotations\Reader;
use JMS\AopBundle\Aop\PointcutInterface;
use Themaholic\CommonBundle\Aop\EntityFlush\EntityFlush as EntityFlush;

class EntityFlushPointcut implements PointcutInterface {

	private $reader;

	public function __construct(Reader $reader)
	{
		$this->reader = $reader;
	}

	public function matchesClass(\ReflectionClass $class)
	{
		return TRUE;
	}

	public function matchesMethod(\ReflectionMethod $method)
	{
		return NULL !== $this->reader->getMethodAnnotation($method, 'Themaholic\CommonBundle\Aop\EntityFlush\EntityFlush');
	}
}

?>