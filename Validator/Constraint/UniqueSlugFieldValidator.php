<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.05.
 * Time: 9:15
 */

namespace Themaholic\CommonBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueSlugFieldValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($constraint->getParentName() == 'library') {
            $repository = $this->entityManager->getRepository('Themaholic\AdminBundle\Entity\Dto\Library');
        }
        elseif ($constraint->getParentName() == 'source') {
            $repository = $this->entityManager->getRepository('Themaholic\AdminBundle\Entity\Dto\Source');
        }

        if ($repository->validateUniqueSlug($value, $constraint->getParentId(), $constraint->getId())) {
            $this->context->addViolation($constraint->message, array());
        }
    }
} 