<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.05.
 * Time: 9:13
 */

namespace Themaholic\CommonBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

class UniqueSlugField extends Constraint
{
    public $message = 'Ez a slug már szerepel egy elemnél, adj meg másikat.';
    public $service = 'Themaholic.validator.unique.slug.field';
    public $em = null;
    public $repositoryMethod = 'findBy';
    public $fields = array();
    public $errorPath = null;
    public $ignoreNull = true;

    protected $id;
    protected $parentId;
    protected $parentName;

    public function construct($options)
    {
        $this->id = $options['id'];
        $this->parentId = $options['parentId'];
        $this->parentName = $options['parentName'];
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParentName()
    {
        return $this->parentName;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    public function getRequiredOptions()
    {
        return array('parentName', 'parentId');
    }

    /**
     * The validator must be defined as a service with this name.
     *
     * @return string
     */
    public function validatedBy()
    {
        return $this->service;
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function getDefaultOption()
    {
        return 'fields';
    }
} 