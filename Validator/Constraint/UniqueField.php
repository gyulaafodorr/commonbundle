<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.05.
 * Time: 9:13
 */

namespace Themaholic\CommonBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint for the Unique Entity validator.
 *
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 *
 * @author Gyula Fodor <gyulaa.fodorr@gmail.com>
 */
class UniqueField extends Constraint
{
    public $message = 'This value is already used.';
    public $service = 'Themaholic.validator.unique.field';
    public $em = null;
    public $repositoryMethod = 'findBy';
    public $fields = array();
    public $errorPath = null;
    public $ignoreNull = true;

    public function getRequiredOptions()
    {
        return array('fields');
    }

    /**
     * The validator must be defined as a service with this name.
     *
     * @return string
     */
    public function validatedBy()
    {
        return $this->service;
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function getDefaultOption()
    {
        return 'fields';
    }
} 