<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.02.
 * Time: 23:32
 */

namespace Themaholic\CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\RouterHelper;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAdminController extends Controller
{
    /**
     * @var FormFactory
     */
    protected $formFactory;
    /**
     * @var RouterHelper
     */
    protected $routingHelper;

    public function setPage(Request $request)
    {
        $request->getSession()->set($this->getPageKey($request), $request->get('page', 1));
        return $this->getRedirectResponse($request);
    }

    public function resetPage(Request $request)
    {
        $request->getSession()->set($this->getPageKey($request), 1);
    }

    public function getPage(Request $request)
    {
        return $request->getSession()->get($this->getPageKey($request), 1);
    }

    public function setOrder(Request $request)
    {
        $request->getSession()->set($this->getOrderKey($request), array('order' => $request->get('order'), 'direction' => $request->get('direction')));
        return $this->getRedirectResponse($request);
    }

    public function getOrder(Request $request)
    {
        return $request->getSession()->get($this->getOrderKey($request), array('order' => '', 'current' => ''));
    }

    public function setFilter(Request $request)
    {
        $filterForm = $this->getFilterForm($request);
        $filterForm->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($filterForm->isValid()) {
                if ($request->get('reset')) {
                    $request->getSession()->set($this->getFilterKey($request), array());
                    $request->getSession()->set($this->getOrderKey($request), array('order' => '', 'current' => ''));
                }
                else {
                    $filterData = $filterForm->getData();
                    $request->getSession()->set($this->getFilterKey($request), $filterData);
                }
                $request->getSession()->set($this->getPageKey($request), 1);
            }
        }
        return $this->getRedirectResponse($request);
    }

    public function getFilter(Request $request)
    {
        return $request->getSession()->get($this->getFilterKey($request), array());
    }

    public function showFilter(Request $request)
    {
        $filterForm = $this->getFilterForm($request);
        return new Response($this->templating->render($this->viewNames['filter'], array('form' => $filterForm->createView())));
    }

    public function getMaxPerPage()
    {
        return 20;
    }

    protected function getFilterKey(Request $request)
    {
        return 'admin/filter/' . self::slugify(get_class($this));
    }

    protected function getPageKey(Request $request)
    {
        return 'admin/page/' . self::slugify(get_class($this));
    }

    protected function getOrderKey(Request $request)
    {
        return 'admin/order/' . self::slugify(get_class($this));
    }

    abstract protected function getFilterForm(Request $request);

    abstract protected function getRedirectResponse(Request $request);

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}