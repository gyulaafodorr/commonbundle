<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 15. 01. 13.
 * Time: 14:38
 */

namespace Themaholic\CommonBundle\Enum;

abstract class AbstractEnum
{
    /**
     * Tombben visszaadja a definialt konstansokat
     *
     * @return array
     */
    public static function getConstants()
    {
        $ref = new \ReflectionClass(get_called_class());
        return $ref->getConstants();
    }

    /**
     * Tombben visszaadja a definialt konstansok neveit
     *
     * @return array
     */
    public static function getConstantNames()
    {
        return array_keys(static::getConstants());
    }

    /**
     * Tombben visszaadja a definialt konstansok ertekeit
     *
     * @return array
     */
    public static function getConstantValues()
    {
        return array_values(static::getConstants());
    }

    /**
     * visszaadja a labelt a konstanshoz
     *
     * @param string $constantName
     * @return stirng
     */
    public static function getLabelByConst($constantName)
    {
        self::validateConstant($constantName);

        $translatedLabelsConstMap = static::getLabelConstMap();
        return $translatedLabelsConstMap[$constantName];
    }

    /**
     * visszaadja a leforditott constans-label tombot
     *
     * @return array
     */
    public static function getLabelConstMap()
    {
        return static::getLabelConstMapConainer();
    }

    /**
     * visszaadja a leforditott cont-label tombot az elejen egy ures sorral
     * @return array
     */
    public static function getLabelConstMapWithEmpty()
    {
        return array('' => '') + static::getLabelConstMapConainer();
    }

    /**
     * tarolja es visszaadja a leforditott tombot
     */
    //abstract protected static function getLabelConstMapConainer();

    /**
     *
     * @param type $constantName
     */
    public static function isValidConstant($constantName)
    {
        $translatedLabelsConstMap = static::getLabelConstMap();
        if (is_array($translatedLabelsConstMap) && isset($translatedLabelsConstMap[$constantName])) return true;

        return false;
    }

    /**
     *
     * @param type $constantName
     * @throws \Exception
     */
    public static function validateConstant($constantName)
    {
        if (!self::isValidConstant($constantName)) {
            throw new \Exception(sprintf('Invalid Enum constant: "%s" @%s', $constantName, get_called_class()));
        }
    }

    /**
     * visszaadja az enuzkm osztaly nevet
     * @return  string
     */
    public static function getClass()
    {
        return get_called_class();
    }
}