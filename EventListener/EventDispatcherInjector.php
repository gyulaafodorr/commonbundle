<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:19
 */

namespace Themaholic\CommonBundle\EventListener;

use Themaholic\CommonBundle\Entity\BaseDomainEntity;
use Doctrine\ORM\Event\LifecycleEventArgs;

class EventDispatcherInjector
{
    /**
     * @var
     */
    private $eventDispatcher;

    /**
     * @param $dispatcher
     */
    public function __construct($dispatcher)
    {
        $this->eventDispatcher = $dispatcher;
    }

    /**
     * @param LifecycleEventArgs $arg
     */
    public function postLoad(LifecycleEventArgs $arg)
    {
        if ($arg->getEntity() instanceOf BaseDomainEntity)
        {
            $arg->getEntity()->setEventDispatcher($this->eventDispatcher);
        }
    }
}