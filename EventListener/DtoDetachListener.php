<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:18
 */

namespace Themaholic\CommonBundle\EventListener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use Themaholic\CommonBundle\Entity\Dto\AbstractDto;

class DtoDetachListener
{
    /**
     * @param PreFlushEventArgs $args
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getIdentityMap() as $entityArray)
        {
            foreach ($entityArray as $entity)
            {
                if ($entity instanceof AbstractDto)
                {
                    $em->detach($entity);
                }
            }
        }
        $uow->computeChangeSets();
    }
}