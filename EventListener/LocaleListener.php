<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.03.22.
 * Time: 11:49
 */

namespace Themaholic\CommonBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession())
        {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $request->attributes->get('_locale'))
        {
            $request->getSession()->set('_locale', $this->getLanguageFromLocale($locale));
        }
        elseif (!$request->getSession()->get('_locale') && $request->getPreferredLanguage())
        {
            $request->setLocale($this->getLanguageFromLocale($request->getPreferredLanguage()));
        }
        else
        {
            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($this->getLanguageFromLocale($request->getSession()->get('_locale', $this->defaultLocale)));
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }

    public function getLanguageFromLocale($locale)
    {
        $language = substr($locale, 0, 2);
        return strlen($language) == 2 ? $language : $this->defaultLocale;
    }

}