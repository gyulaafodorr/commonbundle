<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:18
 */

namespace Themaholic\CommonBundle\EventListener;

use Doctrine\ODM\MongoDB\Event\PreFlushEventArgs;
use Themaholic\CommonBundle\Entity\Dto\AbstractDto;

class DtoDocumentDetachListener
{
    /**
     * @param PreFlushEventArgs $args
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $em = $args->getDocumentManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getIdentityMap() as $entityArray)
        {
            foreach ($entityArray as $entity)
            {
                if ($entity instanceof AbstractDto)
                {
                    $em->detach($entity);
                }
            }
        }
        $uow->computeChangeSets();
    }
}