<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.08.
 * Time: 11:56
 */

namespace Themaholic\CommonBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // AJAX
        if ($event->getRequest()->isXmlHttpRequest()) {
            // redirect
            if ($event->getResponse() && $event->getResponse()->getStatusCode() == 302) {
//                return $event->getResponse();
            }
        }
    }
} 