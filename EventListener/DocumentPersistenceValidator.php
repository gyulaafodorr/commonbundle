<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:20
 */

namespace Themaholic\CommonBundle\EventListener;

use Themaholic\CommonBundle\Exception\DomainPersistanceException;
use Doctrine\ODM\MongoDB\Event\PreFlushEventArgs;

class DocumentPersistenceValidator
{
    /**
     * @var validator
     */
    private $validator;

    public function __construct($validator)
    {
        $this->validator = $validator;
    }

    public function preFlush(PreFlushEventArgs $arg)
    {
        foreach ($arg->getDocumentManager()->getUnitOfWork()->getIdentityMap() as $map)
        {
            foreach ($map as $entity)
            {
                $errors = $this->validator->validate($entity);
                if ($errors->count() > 0)
                {
                    throw new DomainPersistanceException($errors);
                }
            }
        }
    }
}