<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:18
 */

namespace Themaholic\CommonBundle\EventListener;

use Doctrine\ODM\MongoDB\Event\PostFlushEventArgs;
use Themaholic\CommonBundle\Entity\BaseDomainEntity;

class DocumentDomainEventListener
{
    /**
     * @var array
     */
    protected $entities = array();

    /**
     * @param PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $em = $args->getDocumentManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getIdentityMap() as $entityArray)
        {
            foreach ($entityArray as $entity)
            {
                if ($entity instanceof BaseDomainEntity)
                {
                    $this->entities[] = $entity;
                }
            }
        }

        // process event
        $this->processEvents();
    }

    protected function processEvents()
    {
//        foreach ($this->entities as $entity)
//        {
//            foreach ($entity->popEvents() as $event)
//            {
//
//            }
//        }
    }

}