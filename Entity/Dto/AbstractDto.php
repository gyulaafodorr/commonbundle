<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:12
 */

namespace Themaholic\CommonBundle\Entity\Dto;

abstract class AbstractDto
{
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public final function getClass()
    {
        $class = explode("\\", get_class($this));
        return array_pop($class);
    }

    /**
     * @return string
     */
    public final function getNamespace()
    {
        $class = explode("\\", get_class($this));
        array_pop($class);
        return implode("\\", $class);
    }
} 