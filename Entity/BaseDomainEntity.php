<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.02.02.
 * Time: 21:15
 */

namespace Themaholic\CommonBundle\Entity;

use Themaholic\CommonBundle\Event\DomainEventInterface;

abstract class BaseDomainEntity
{
    protected $id;
    protected $dispatcher;
    private $events = array();

    public function popEvents()
    {
        $events = $this->events;
        $this->events = array();

        return $events;
    }

    protected function raise(DomainEventInterface $event)
    {
        $this->events[] = $event;
    }

    public function setEventDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public final function getClass()
    {
        $class = explode("\\", get_class($this));
        return array_pop($class);
    }

    /**
     * @return string
     */
    public final function getNamespace()
    {
        $class = explode("\\", get_class($this));
        array_pop($class);
        return implode("\\", $class);
    }
} 