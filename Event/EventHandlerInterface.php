<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.10.
 * Time: 20:33
 */

namespace Themaholic\CommonBundle\Event;


interface EventHandlerInterface
{
    public function handleEvent(EventInterface $event);
}