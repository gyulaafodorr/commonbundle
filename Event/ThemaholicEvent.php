<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 02/08/13
 * Time: 14:50
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Event;

use Symfony\Component\EventDispatcher\Event;

abstract class ThemaholicEvent extends Event implements EventInterface
{
    /**
     * @var string
     */
    protected $eventName;

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

}