<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gyula
 * Date: 05/06/13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */

namespace Themaholic\CommonBundle\Event;

abstract class AbstractDomainEvent implements DomainEventInterface
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @param array $params
     */
    public function __construct($params = array())
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    abstract public function getName();

}