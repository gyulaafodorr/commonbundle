<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.10.
 * Time: 20:28
 */

namespace Themaholic\CommonBundle\Event;

interface EventInterface
{
    public function getEventName();
} 