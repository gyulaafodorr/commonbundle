<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.03.22.
 * Time: 19:49
 */

namespace Themaholic\CommonBundle\Event;

interface DomainEventInterface
{
    public function __construct($params = array());
    public function getParams();
    public function getName();
} 