<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.10.
 * Time: 20:30
 */

namespace Themaholic\CommonBundle\Event;


interface EventDispatcherInterface
{
    public static function raise(EventInterface $event);
}