<?php
/**
 * Created by PhpStorm.
 * User: gyula
 * Date: 2014.06.10.
 * Time: 20:31
 */

namespace Themaholic\CommonBundle\Event;


class EventDispatcher implements EventDispatcherInterface
{
    /**
     * @var EventHandlerInterface
     */
    private static $eventHandler;

    public static function setEventHandler(EventHandlerInterface $handler)
    {
        self::$eventHandler = $handler;
    }

    public static function raise(EventInterface $event)
    {
        return self::$eventHandler->handleEvent($event);
    }

} 